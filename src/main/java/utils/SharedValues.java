package utils;

import java.util.UUID;

public class SharedValues {

	private static int NumeroFilas ; // numero de ingenios
	private static int NumeroColumnas; // numero de parcelas
	private static int Iteraciones;
	private static int MaxNumIteraciones; // maximo numero de interaciones 
	private static UUID idGuardado;
	private static double CapacidadMaxima;
	

	public static double getCapacidadMaxima() {
		return CapacidadMaxima;
	}
	public static void setCapacidadMaxima(double capacidadMaxima) {
		CapacidadMaxima = capacidadMaxima;
	}
	public static UUID getIdGuardado() {
		return idGuardado;
	}
	public static void setIdGuardado(UUID idGuardado) {
		SharedValues.idGuardado = idGuardado;
	}
	public static int getMaxNumIteraciones() {
		return MaxNumIteraciones;
	}
	public static void setMaxNumIteraciones(int maxNumIteraciones) {
		MaxNumIteraciones = maxNumIteraciones;
	}
	public static int getIteraciones() {
		return Iteraciones;
	}
	public static void setIteraciones(int iteraciones) {
		Iteraciones = iteraciones;
	}
	public static int getNumeroFilas() {
		return NumeroFilas;
	}
	public static void setNumeroFilas(int numeroFilas) {
		NumeroFilas = numeroFilas;
	}
	public static int getNumeroColumnas() {
		return NumeroColumnas;
	}
	public static void setNumeroColumnas(int numeroColumnas) {
		NumeroColumnas = numeroColumnas;
	}
	
	
	
}

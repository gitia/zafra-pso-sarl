# Overview #

This repo contains a Particle Swarm Optimization (PSO) algorithm implemented with Multiagent Systems (SMA), using general purpose agent-oriented programming language SARL (http://www.sarl.io).

The algorithm is intended to solve a particular variant of the Generalized Assignment Problem (GAP), called Variable Profit GAP (VPGAP).
More details of the variant and the algorithm can be found in a paper to be published soon in proceedings of VIII International Conference Optimization and Applications (OPTIMA-2017).
This paper should be used for future references. 

# Requirements #

Java Development ToolKit (JDK) 1.8 or higher.

SARL Development Environment Version 0.5.7 (http://www.sarl.io)

Maven 3.0 or higher.

Once the repo is cloned, it must be imported into SARL.
Then Run Application should be configured selecting agent "Swarm" as project main class.

# Sample data #
Sample data files are located in data directory. 
They are the same as in http://www.gitia.org/projects/vpgap/, where you can find an explanation of the format, and tools to generate new samples.


If you have any further questions, please write to pedro.araujo@gitia.org or nicolas.majorel@gitia.org.

